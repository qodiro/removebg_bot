const { Scenes } = require("telegraf");
const scene = new Scenes.BaseScene("start");
const FormData = require("form-data");
const axios = require("axios");

const removeBg = async function (url) {
  const formData = new FormData();
  formData.append("image_url", url);
  const res = await axios.post(
    "https://api.remove.bg/v1.0/removebg",
    formData,
    {
      headers: {
        ...formData.getHeaders(),
        "X-Api-Key": "n1tcyF39viRnqn3AAFnnGs6h"
      },
      responseType: "arraybuffer"
    }
  );
  return res.data;
};

scene.enter((ctx) => {
  ctx.reply(
    `👋 Salom ${ctx.from.first_name} men rasmdagi orqa foni o'chiradigan botman. 🌆 Fotoni jo'nating .. `
  );
});



scene.on("photo", async (ctx) => {
  const file_id =
    ctx.update.message.photo[ctx.update.message.photo.length - 1].file_id;
  const file_path = (await ctx.telegram.getFile(file_id)).file_path;
  const url = `https://api.telegram.org/file/bot${process.env.BOT_TOKEN}/${file_path}`;

  const photo = await removeBg(url);
  ctx.replyWithDocument({ source: photo, filename: "test.png" });
});

module.exports = scene;