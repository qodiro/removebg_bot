const { checkSubscript } = require("./middlewares/checksubsribtion");
const { session } = require("telegraf");

const { bot } = require("./core/bot");
const stage = require("./scenes/index");

bot.use(session());
bot.use(stage.middleware());
bot.use(checkSubscript);
bot.start((ctx) => ctx.scene.enter("start"));

bot.action("check_subscription", async (ctx) => {
  let channel = "dostondev321";
  const chatMember = await ctx.telegram.getChatMember(
    `@${channel}`,
    ctx.from.id
  );
  var isSubscribed = ["administrator", "member", "owner"].includes(
    chatMember.status
  );
  ctx.deleteMessage();
  if (isSubscribed) {
    ctx.scene.enter("start");
  } else {
    ctx.reply(`Botdan foydalanish uchun
  @${channel} kanaliga a'zo bo'lishingiz shart!`);
  }
});

bot.launch();