require("dotenv").config();
// const chan = process.env.CHANNAL;

const checkSubscript = async (ctx, next) => {
  let channel = 'dostondev321';
  const chatMember = await ctx.telegram.getChatMember(`@${channel}`, ctx.from.id);
  var isSubscribed = ["administrator", "creator", "member", "owner"].includes(
    chatMember.status
  );
  if (ctx.callbackQuery) {
    ctx.answerCbQuery();
  }
  if (isSubscribed) {
    next();
  } else {
    ctx.reply(
      `
    <b>Xush kelibsiz!</b>
Bizning botimizdan foydalanayotganingizdan xursandmiz.

🗣️ Botdan foydalanish uchun kanalimizga obuna bo’lishingiz kerak 

Kanalga a'zo bo'lganingizdan so'ng
✅ Tekshirib ko'rish tugmasinini bosing.
    `,
      {
        parse_mode: "HTMl",
        reply_markup: {
          inline_keyboard: [
            [{ text: "Kanalga o'tish", url: `https://t.me/${channel}` }],
            [
              {
                text: "✅ Tekshirib ko'rish",
                callback_data: "check_subscription"
              }
            ]
          ]
        }
      }
    );
  }
};

module.exports = { checkSubscript };
